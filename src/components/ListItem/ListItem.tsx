import React from "react";
import { Customer} from "../../types";
import "./ListItem.css";
import Avatar from "../Avatar/Avatar";



interface Props {
  customer: Customer;
}

const ListItem: React.FC<Props> = ({ customer }) => {
  let class_name:string="listItemActive";
  if(!customer.active){
    class_name="listItemInActive";
  }
  return (
    <li className={class_name}>
      <div className={"listItemContent"}>
        <div className={"avatar"}>
          <Avatar name={customer.name} url={customer.profileImage} />
        </div>
        <div className={"content"}>
          <div className={"title"}>{customer.name}</div>
          <div className={"subtitle"}>
            <div>{customer.email}</div>
            <div>{customer.phone}</div>
          </div>
        </div>
      </div>
      <div className={"inactiveTag"}>
        <div>{customer.active?"active":"inactive"}</div>
      </div>
    </li>
  );
};

export default ListItem;
